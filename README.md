# Ansible opendkim

Ansible role for installing and configuring OpenDKIM mail processor.

Currently made to work with CentOS 8 stream Linux.

## Project status
This is a work in progress. I am using this to manage my own private servers and I will contribute as time allows.

You may use this code if you find it useful.

## Dependencies

Requires dw.postfix: https://gitlab.com/dustwolf/dw.postfix
